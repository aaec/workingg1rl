" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" Author: Ava Cole <aaec@uw.edu>
" Source: gitlab.com/aaec/workingg1rl
" License: MIT
" Description: this airline theme is for WORRKDING girls ONLY
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

" Initialize the pallete
let g:airline#themes#workingg1rl#palette = {}

"retrieve colors from main colorscheme highlight groups
let s:Pink0UI = airline#themes#get_highlight('Pink0UI')
let s:Pink1UI = airline#themes#get_highlight('Pink1UI')
let s:Blue0UI = airline#themes#get_highlight('Blue0UI')
let s:Blue1UI = airline#themes#get_highlight('Blue1UI')
let s:Red0UI = airline#themes#get_highlight('Red0UI')
let s:Red1UI = airline#themes#get_highlight('Red1UI')
let s:Yellow0UI = airline#themes#get_highlight('Yellow0UI')
let s:Yellow1UI = airline#themes#get_highlight('Yellow1UI')
let s:cyan0UI = airline#themes#get_highlight('cyan0UI')
let s:cyan1UI = airline#themes#get_highlight('cyan1UI')

let s:Base0UI = airline#themes#get_highlight('Base0UI')
let s:Base3UI = airline#themes#get_highlight('Base3UI')
let s:TabNormal = airline#themes#get_highlight('TabNormal')
let s:TabModified = airline#themes#get_highlight('TabModified')
let s:TabSelected = airline#themes#get_highlight('Base4Bold')

let s:Warning = airline#themes#get_highlight('Warning')
let s:Error = airline#themes#get_highlight('Error')

"set the colors for each mode.
"airline bar is split up into 6 sections from left to right, generate color map
"takes three colors and mirrors them accross the bar
let g:airline#themes#workingg1rl#palette.normal = airline#themes#generate_color_map(s:Pink0UI, s:Pink1UI, s:Base0UI)
let g:airline#themes#workingg1rl#palette.insert = airline#themes#generate_color_map(s:Blue0UI, s:Blue1UI, s:Base0UI)
let g:airline#themes#workingg1rl#palette.replace = airline#themes#generate_color_map(s:Red0UI, s:Red1UI, s:Base0UI)
let g:airline#themes#workingg1rl#palette.visual = airline#themes#generate_color_map(s:Yellow0UI, s:Yellow1UI, s:Base0UI)
let g:airline#themes#workingg1rl#palette.commandline = airline#themes#generate_color_map(s:cyan0UI, s:cyan1UI, s:Base0UI)
let g:airline#themes#workingg1rl#palette.inactive = airline#themes#generate_color_map(s:Base3UI, s:Base3UI, s:Base3UI)

"apply warning and error to all of the groups
for item in ['normal', 'insert', 'replace', 'visual', 'commandline']
	execute "let g:airline#themes#workingg1rl#palette.".item.".airline_warning = s:Warning"
	execute "let g:airline#themes#workingg1rl#palette.".item.".airline_error = s:Error"
endfor

"make the tabline pretty
let g:airline#themes#workingg1rl#palette.tabline = {
	\ 'airline_tab': s:TabNormal,
	\ 'airline_tabsel': s:TabSelected,
	\ 'airline_tabtype': s:TabNormal,
	\ 'airline_tabfill': s:TabNormal,
	\ 'airline_tabhid': s:TabNormal,
	\ 'airline_tabmod': s:TabModified
	\ }

"accents are used for stuff like the read only flag
let g:airline#themes#workingg1rl#palette.accents = {
      \ 'red': [ '#ff0000' , '' , 160 , ''  ]
      \ }
