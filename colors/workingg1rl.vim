" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" Author: Ava Cole <aaec@uw.edu>
" Source: gitlab.com/aaec/workingg1rl
" License: MIT
" Description: this theme is for WORRKDING girls ONLY
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"TODO:
"- actually put effort into light mode
"- do more syntax research, add better language support
"- do more plugin research
" Theme Setup: {{{

if version > 580
	highlight clear
	if exists("syntax_on")
		syntax reset
	endif
endif

"this theme is for WORKIN G!RL55 only!!!
let g:colors_name="workingg1rl"

if !(has('termguicolors') && &termguicolors) && !has('gui_running') && &t_Co != 256
	let s:use_term_colors=1
endif

"}}}
" Color Inspo Zone: {{{

"pure blue       #00eff7
"pure pink       #de0094
"pure yellow     #ffc700
"pure green      #00e860
"pure red/orange #ff0033
"pure cyan       #bbff00

"}}}
let s:dark00  = ['#232323',235]
let s:dark0   = ['#2b2f32',236]
let s:dark1   = ['#34393c',239]
let s:dark2   = ['#420f31', 15] "fix
let s:dark3   = ['#7C807C',244]
let s:dark4   = ['#d9e2d9',253]
let s:light00 = ['#b1b4b1',249]
let s:light0  = ['#dce1dc',253]
let s:light1  = ['#f2f7f2',255]
let s:light2  = ['#f3f2f0',255] "fix
let s:light3  = ['#6e716e',242]
let s:light4  = ['#363836',237]

let s:blue0   = ['#00575a', 23]
let s:blue1   = ['#33f0f7', 87]
let s:blue2   = ['#73f6fa', 87]
let s:yellow1 = ['#edc63b',221]
let s:yellow2 = ['#f4d975',222]
let s:pink0   = ['#510036', 53]
let s:pink1   = ['#e026a2',163]
let s:pink2   = ['#f08bce',212]
let s:green1  = ['#5de294', 78]
let s:green2  = ['#7ae7a7', 78]
let s:red0    = ['#720e22', 89]
let s:red1    = ['#ff496e',203]
let s:red2    = ['#ea8a9d',175]
let s:cyan1   = ['#859360',101]
let s:cyan2   = ['#c2d68b',150]
" Global Settings: {{{

" If terminal only has 16 colors use them
if s:use_term_colors ==# 1
	let s:dark00[1]   = 8
	let s:dark0[1]    = 0
	let s:dark1[1]    = 0
	let s:dark2[1]    = 8
	let s:dark3[1]    = 15
	let s:dark4[1]    = 7
	let s:blue0[1]    = 4
	let s:blue1[1]    = 4
	let s:blue2[1]    = 12
	let s:yellow1[1]  = 3
	let s:yellow2[1]  = 11
	let s:pink0[1]    = 5
	let s:pink1[1]    = 5
	let s:pink2[1]    = 13
	let s:green1[1]   = 2
	let s:green2[1]   = 10
	let s:red0[1]     = 1
	let s:red1[1]     = 1
	let s:red2[1]     = 9
	let s:cyan1[1]    = 6
	let s:cyan2[1]    = 14
endif

" Initialize global settings if the user hasn't specified them
if !exists('g:theme_bold')
	let g:theme_bold=1
endif
if !exists('g:theme_italic')
	if has("gui_running") || $TERM_ITALICS ==? "true"
		let g:theme_italic=1
	else
		let g:theme_italic=0
	endif
endif
if !exists('g:theme_underline')
	let g:theme_underline=1
endif
if !exists('g:theme_inverse')
	let g:theme_inverse=1
endif
if !exists('g:theme_undercurl')
	let g:theme_undercurl=1
endif
if !exists('g:meskag1rl_status')
	let g:meskag1rl_status=0
endif
let s:is_light = (&background ==# 'light')

"decides weather theme will use gui or cterm colors
if has('gui_running') || (has('termguicolors') && &termguicolors)
	let s:color_list_index = 0
else
	let s:color_list_index = 1
endif

" Configure local variables to reflect global settings
let s:bold = g:theme_bold ==# 1 ? 'bold' : ''
let s:italic = g:theme_italic ==# 1 ? 'italic' : ''
let s:underline = g:theme_underline ==# 1 ? 'underline' : ''
let s:inverse = g:theme_inverse ==# 1 ? 'inverse' : ''
let s:undercurl = g:theme_undercurl ==# 1 ? 'undercurl' : ''

" }}}
" Functions:{{{

" sets highlights for a:group, optional param is style (i.e. bold)
" (see :h highlight)
function! SetHighlight(group, bg, fg, ...)
	let hlstring = 'highlight ' . a:group . ' '

	if a:bg[0] !=? ''
		let hlstring .= 'guibg=' . a:bg[0] . ' '
		let hlstring .= 'ctermbg=' . a:bg[1] . ' '
	endif

	if a:fg[0] !=? ''
		let hlstring .= 'guifg=' . a:fg[0] . ' '
		let hlstring .= 'ctermfg=' . a:fg[1] . ' '
	endif

	if a:0 >= 1 && a:1 !=? ''
		let hlstring .= 'cterm=' . a:1
	endif

	execute hlstring
endfunction

" }}}
" Highlight Groups: {{{

if s:is_light
	"{{{
	"normal can't be linked to other groups so we do it here
	call SetHighlight('Normal',s:light1, s:light4)
	call SetHighlight('JustUnderline','', '', s:underline)
	"base 4 is for normal text on normal background (e.g. light4 on light1)
	call SetHighlight('Base4Plain',s:light1, s:light4)
	call SetHighlight('Base4Bold',s:light1, s:light4, s:bold)
	call SetHighlight('Base4Italic',s:light1, s:light4, s:italic)
	call SetHighlight('Base4Inverse',s:light1, s:light4, s:inverse)
	"base 3 is lower contrast than base 4, used for comments and linenr
	call SetHighlight('Base3Plain',s:light1, s:light3)
	call SetHighlight('Base3Italic',s:light1, s:light3, s:italic)
	call SetHighlight('Base3Inverse',s:light1, s:light3, s:inverse)
	call SetHighlight('Base3Underline',s:light1, s:light3, s:underline)
	"base 2 is for background accents and nontext chars and things
	call SetHighlight('Base2Plain',s:light1, s:light2)
	"base 0 is darker than base1 background, used mostly for ui
	"these color sections all use the normal background color
	call SetHighlight('Red',s:light1, s:red1)
	"yeah so in light mode we just use cyan instead of yellow becuase yellow on
	"white is unreadable
	call SetHighlight('Yellow',s:light1, s:cyan1)
	call SetHighlight('Blue',s:light1, s:blue0)
	call SetHighlight('BlueItalic',s:light1, s:blue0, s:italic)
	call SetHighlight('Green',s:light1, s:green1)
	call SetHighlight('Pink',s:light1, s:pink0)

	call SetHighlight('RedHighlighted',s:red2, s:light4)
	call SetHighlight('GreenHighlighted',s:cyan2, s:light4)
	call SetHighlight('YellowHighlighted',s:yellow2, s:light4)
	"ui elements
	call SetHighlight('Base0UI',s:light0, s:light4)
	call SetHighlight('Base3UI',s:light0, s:light3)
	call SetHighlight('TabNormal',s:light00, s:light4)
	call SetHighlight('TabModified',s:light1, s:red1)
	call SetHighlight('Error',s:red1, s:light4)
	call SetHighlight('Warning',s:light0, s:red2)
	"}}}
else " is dark
	"{{{
	"normal can't be linked to other groups so we do it here
	call SetHighlight('Normal',s:dark1, s:dark4)
	call SetHighlight('JustUnderline','', '', s:underline)
	"base 4 is for normal text on normal background (e.g. dark4 on dark1)
	call SetHighlight('Base4Plain',s:dark1, s:dark4)
	call SetHighlight('Base4Bold',s:dark1, s:dark4, s:bold)
	call SetHighlight('Base4Italic',s:dark1, s:dark4, s:italic)
	call SetHighlight('Base4Inverse',s:dark1, s:dark4, s:inverse)
	"base 3 is lower contrast than base 4, used for comments and linenr
	call SetHighlight('Base3Plain',s:dark1, s:dark3)
	call SetHighlight('Base3Italic',s:dark1, s:dark3, s:italic)
	call SetHighlight('Base3Inverse',s:dark1, s:dark3, s:inverse)
	call SetHighlight('Base3Underline',s:dark1, s:dark3, s:underline)
	"base 2 is for background accents and nontext chars and things
	call SetHighlight('Base2Plain',s:dark1, s:dark2)
	"these color sections all use the normal background color
	call SetHighlight('Red',s:dark1, s:red1)
	call SetHighlight('Yellow',s:dark1, s:yellow1)
	call SetHighlight('Blue',s:dark1, s:blue1)
	call SetHighlight('BlueItalic',s:dark1, s:blue1, s:italic)
	call SetHighlight('Green',s:dark1, s:green1)
	call SetHighlight('Pink',s:dark1, s:pink1)

	call SetHighlight('RedHighlighted',s:red2, s:dark0)
	call SetHighlight('GreenHighlighted',s:cyan2, s:dark0)
	call SetHighlight('YellowHighlighted',s:yellow2, s:dark0)

	" ui elements
	call SetHighlight('Base0UI',s:dark0, s:dark4)
	call SetHighlight('Base3UI',s:dark0, s:dark3)
	call SetHighlight('TabNormal',s:dark00, s:dark3)
	call SetHighlight('TabModified',s:dark1, s:pink2)
	call SetHighlight('Error',s:red1, s:dark0)
	call SetHighlight('Warning',s:dark0, s:red2)
	"}}}
endif
call SetHighlight('Pink0UI', s:pink0, s:dark4)
call SetHighlight('Pink1UI', s:pink1, s:dark4)
call SetHighlight('Blue0UI', s:blue0, s:dark4)
call SetHighlight('Blue1UI', s:blue2, s:dark0)
call SetHighlight('Yellow0UI', s:dark0, s:yellow2)
call SetHighlight('Yellow1UI', s:yellow2, s:dark0)
call SetHighlight('Red0UI', s:red0, s:dark4)
call SetHighlight('Red1UI', s:red1, s:dark4)
call SetHighlight('cyan0UI', s:dark0, s:cyan2)
call SetHighlight('cyan1UI', s:cyan2, s:dark0)

" }}}
" Base UI and Syntax:{{{

"see :h group-name for syntax groups
"see :h highlight-groups for ui groups
"Normal text, Folded, list chars {{{

highlight! link Folded Base4Bold
"special key is the group that listchars (your tab indicator) hl's
highlight! link SpecialKey Base2Plain
highlight! link NonText Base2Plain

"}}}
"Gutter {{{

highlight! link LineNr Base3Plain
highlight! link CursorLineNr Base3Inverse
"sign column is the col on the far left
highlight! link SignColumn Base4Plain
"fold column apperently is a thing too
highlight! link FoldColumn Base3Plain

"}}}
" general UI {{{

highlight! link StatusLineNC Base3Plain
highlight! link VertSplit Base3Plain
" this is all overwriten by airline but active tab looks like its part of the
" page while unactive blends in with background
highlight! link TabLine TabLineFill
highlight! link TabLineSel Normal

highlight! link Directory Blue

" }}}
" Meskarune Statusline: {{{

if g:meskag1rl_status ==# 1

	" Functions: {{{
	" Automatically change the statusline color depending on mode - doesn't update until cursor changes position
	function! g:workingg1rl#ChangeStatuslineColor()
		if (mode() =~# '\v(n|no)')
			call SetHighlight("statusline", s:dark0, s:pink0)
		elseif (mode() =~# '\v(v|V)' || g:workingg1rl#currentmode[mode()] ==# 'V·Block')
			call SetHighlight("statusline", s:dark0, s:yellow1)
		elseif (mode() ==# 'R')
			call SetHighlight("statusline", s:dark0, s:red0)
		elseif (mode() ==# 'i')
			call SetHighlight("statusline", s:dark0, s:blue0)
		else
			call SetHighlight("statusline", s:dark0, s:pink0)
		endif
		return ''
	endfunction

	function! g:workingg1rl#StatuslineTrailingSpaceWarning()
		if !exists("b:statusline_trailing_space_warning")
			if search('\s\+$', 'nw') != 0
				let b:statusline_trailing_space_warning = '[\s]'
			else
				let b:statusline_trailing_space_warning = ''
			endif
		endif
		return b:statusline_trailing_space_warning
	endfunction
	" }}}

	" Changes the color for intering and exiting instert mode immediately
	au InsertEnter * call SetHighlight("statusline", s:dark0, s:blue0)
	au InsertLeave * call SetHighlight("statusline", s:dark0, s:pink0)
	call SetHighlight("statusline", s:dark0, s:pink0)

	autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

	" Status Line Custom
	let g:workingg1rl#currentmode={
		\ 'n'  : 'Normal',
		\ 'no' : 'Normal·Operator Pending',
		\ 'v' : 'Visual',
		\ 'V' : 'V·Line',
		\ "\<c-V>" : 'V·Block',
		\ 's'  : 'Select',
		\ 'S'  : 'S·Line',
		\ "\<c-S>" : 'S·Block',
		\ 'i'  : 'Insert',
		\ 'R'  : 'Replace',
		\ 'Rv' : 'V·Replace',
		\ 'c'  : 'Command',
		\ 'cv' : 'Vim Ex',
		\ 'ce' : 'Ex',
		\ 'r'  : 'Prompt',
		\ 'rm' : 'More',
		\ 'r?' : 'Confirm',
		\ '!'  : 'Shell',
		\ 't'  : 'Terminal'
		\}

	set laststatus=2
	set noshowmode
	" Change the statusline color
	set statusline=
	set statusline=%{g:workingg1rl#ChangeStatuslineColor()}
	set statusline+=%1*\ %n\                                    " Buffer number
    " The current mode
	set statusline+=%0*\ %{toupper(g:workingg1rl#currentmode[mode()])}\ "<- leave
	set statusline+=%1*\ %<%t\                                  " Tail of file path
	set statusline+=%3*                                         " Separator
    " modified, readonly, trailing space
	set statusline+=%4*%M%r%{g:workingg1rl#StatuslineTrailingSpaceWarning()}
	set statusline+=%2*%h%w                                     " Helpfile, preview
	set statusline+=%=                                          " Right Side
	set statusline+=%3*                                         " Separator
	set statusline+=%1*\ %Y                                     " FileType
	set statusline+=\ [%{''.(&fenc!=''?&fenc:&enc).''}]\        " Encoding
	set statusline+=%0*\ ≡\ %02l/%L\                   " See next line
	"set statusline+=%1*\ ln:\ %02l/%L\ (%3p%%)\          " See next line
	" Line number / total lines, col number, percentage of document

	call SetHighlight("User1", s:dark3, s:dark0)

endif
" }}}
"syntax {{{

"Comments
highlight! link Comment Base3Italic
"Constants
"any other kind of constant
highlight! link Constant Red
highlight! link String Yellow
highlight! link Character Red
highlight! link Number Red
highlight! link Float Red
highlight! link Boolean Red
"Identifiers
""any variable name
highlight! link Identifier Blue
"function names or methods for classes
highlight! link Function Green
"Statements
"any other statements
highlight! link Statement Base4Italic
"if, then, else, switch, etc
highlight! link Conditional Pink
"for, do, while, etc
highlight! link Repeat Pink
"case, default, etc
highlight! link Label Pink
"sizeof, +, *, %, etc
highlight! link Operator Base4Plain
"any other keyword
highlight! link Keyword Pink
"try, catch, throw, etc
highlight! link Exception Pink
"PreProcs
highlight! link PreProc Pink
"Types
"int, long, char, etc
highlight! link Type BlueItalic
"static, register, volatile, etc
highlight! link StorageClass Red
"struct, union, enum, etc
highlight! link Structure Green
"highlight! link Typedef Type "type definition, implicitly linked rn
"Specials
"any special symbol whatever that means
highlight! link Special Base4Plain
"call SetHighlight('SpecialChar', '', s:base4) "special chars in constants
"call SetHighlight('Tag', '', s:base4) "tags?
"call SetHighlight('Delimiter', '', s:base4) "'char that needs attention'
"call SetHighlight('SpecialComment', '', s:base4) "special things
"call SetHighlight('Debug', '', s:base4) "debugging statements
"Extra Specials
"stuff like HTML links
highlight! link Underline JustUnderline
"call SetHighlight('Ignore', '', s:blue, s:underline) "stuff in hl-Ignore
"call SetHighlight('Error', '', s:blue, s:underline)
highlight! link Todo Base3Underline

"}}}
" search, cursor, etc {{{

" incSearch normally just inverts bg and fg and now search does that too
"call SetHighlight('incSearch', '', '', s:underline)
highlight! link Search Base3verse
" link all the different cursors together
highlight! link vCursor Cursor
highlight! link iCursor Cursor
highlight! link lCursor Cursor
"highlight! link CursorLine Base0
"makes matchparen less annoying by underlining instead of inverting
if s:underline !=# ''
	highlight! link MatchParen JustUnderline
endif
"setting that sets color for the 81st col thing
"call SetHighlight('ColorColumn', 'NONE', 'NONE', s:inverse)

" }}}
" Popup Menu: {{{

highlight! link Pmenu Base0UI
highlight! link PmenuSel Pink1UI
highlight! link PmenuSbar TabNormal
highlight! link PmenuThumb Pink0UI
" }}}
" Diffs: {{{

 highlight! link DiffAdd GreenHighlighted
 highlight! link DiffChange YellowHighlighted
 highlight! link DiffDelete RedHighlighted

" }}}
" Spelling: {{{

highlight! link SpellBad JustUnderline
highlight! link SpellCap Normal
highlight! link SpellLocal Normal
highlight! link SpellRare Normal

" }}}

"}}}
" Plugins: {{{

" ALE: {{{
"todo
" }}}
 " NERDTree {{{

	highlight! link NERDTreeDir Blue
	highlight! link NERDTreeDirSlash Blue
	highlight! link NERDTreeOpenable Base4Plain
	highlight! link NERDTreeClosable Base4Plain
	highlight! link NERDTreeFile Base4Plain
	highlight! link NERDTreeExecFile Yellow
	highlight! link NERDTreeUp Base3Plain
	highlight! link NERDTreeCWD Green
	highlight! link NERDTreeHelp Base4Plain

 " }}}

" }}}
" Language Specific: {{{

" Vim {{{

call SetHighlight('vimCommentTitle', '', '', s:bold)
highlight! link vimNotation Yellow
"highlight! link vimBracket GruvboxOrange
highlight! link vimMapModKey Base4Plain
highlight! link vimFuncSID Base4Plain

" }}}
" Python {{{

highlight! link pythonBuiltin Blue
highlight! link pythonBuiltinObj Blue
highlight! link pythonBuiltinFunc Blue
highlight! link pythonFunction Green
highlight! link pythonDecorator Pink
highlight! link pythonInclude Blue
highlight! link pythonImport Blue
highlight! link pythonRun Blue
highlight! link pythonCoding Blue
highlight! link pythonOperator Base4Plain
highlight! link pythonException Pink
highlight! link pythonExceptions Pink
highlight! link pythonBoolean Red
highlight! link pythonDot Base4Plain
highlight! link pythonConditional Pink
highlight! link pythonRepeat Pink
highlight! link pythonDottedName Green

" }}}
" Java: {{{

highlight! link javaAnnotation Blue
highlight! link javaDocTags Base3Underline
highlight! link javaCommentTitle Base3Bold
highlight! link javaParen Base4Plain
highlight! link javaParen1 Base4Plain
highlight! link javaParen2 Base4Plain
highlight! link javaParen3 Base4Plain
highlight! link javaParen4 Base4Plain
highlight! link javaParen5 Base4Plain
highlight! link javaVarArg Green

"}}}

" }}}
