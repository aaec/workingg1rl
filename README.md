# WORKING G1RL
> A fun and professional colorscheme for vim 
![screenshot](screenshot1.png "Screenshot One")
![screenshot](Screenshot2.png "Screenshot Two")


## Features
- Dark and light schemes.
- Language and Plugin specific syntax.
- Works with true color term emulators like GNOME term, 256 color terms like xterm, and real 1337 16 color terms like /dev/tty.
- Readable code. Contribution encouraged!
- The only vim themed designed for r34l w0rld w0rking G1RLS!

## Usage
Installation in vim is as easy as adding `Plugin "https://gitlab.com/aaec/workingg1rl.git"` or equivalent to your .vimrc.
By default the scheme will try to use bold, italic, inverse, and underlined styles. If you want manually turn any of these off, put one of these in your .vimrc. 
```VimL
let g:theme_bold = 0
let g:theme_italic = 0
let g:theme_inverse = 0
let g:theme_underline = 0
```
Additionally there is a cool status line designed by github.com/meskarune that is disabled by default.
It can be enabled with:
```VimL
let g:meskag1rl_status = 1
```
## ToDo
Moving forward I want to:
- Make the light theme better.
- Add more plugin and language specific theme-ing. If you use it and make modifications to support a specific use, please make a pull request!

## Credit
Big shouts out to [Dracula](https://draculatheme.com/), [Sierra](https://github.com/AlessandroYorba/Sierra), and of course [Solarized](https://ethanschoonover.com/solarized/) for inspiration.
